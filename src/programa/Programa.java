package programa;

import java.time.LocalDate;

import java.util.Scanner;

import clases.Festivales;
import clases.Responsable;
import clases.Trabajo;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		Festivales festival = new Festivales();

		int opcion;
		do {
			System.out.println("Menu");
			System.out.println("1- Dar de alta elementos de Clase1");
			System.out.println("2- Buscar elementos de Clase1");
			System.out.println("3- Eliminamos elementos de Clase1");
			System.out.println("4- Listamos elementos de Clase1");
			System.out.println("5- Dar de alta elementos de Clas2");
			System.out.println("6- Buscar elementos de Clase2");
			System.out.println("7- Eliminamos elementos de Clase2");
			System.out.println("8- Listamos elementos de Clase2 por algun atributo");
			System.out.println("9- Listamos elementos de Clase2 con algun elemento de Clase1");
			System.out.println("10- Asignamos elementos que usen Clase1 y Clase2 y listamos");

			System.out.print("Introduce una opcion: ");
			opcion = input.nextInt();
			input.nextLine();

			switch (opcion) {

			case 1:

				System.out.println("Damos de alta a tres elementos de clase1");

				System.out.println("Introduce primer responsable");
				Responsable responsable1 = new Responsable();
				System.out.println("Introduce DNI: ");
				String dni = input.nextLine();

				System.out.println("Introduce nombre: ");
				String nombre = input.nextLine();

				responsable1.setDni(dni);
				responsable1.setNombre(nombre);

				System.out.println("Introduce segundo responsable");
				Responsable responsable2 = new Responsable();
				System.out.println("Introduce DNI: ");
				String dni2 = input.nextLine();

				System.out.println("Introduce nombre: ");
				String nombre2 = input.nextLine();

				responsable2.setDni(dni2);
				responsable2.setNombre(nombre2);

				System.out.println("Introduce tercer responsable");
				Responsable responsable3 = new Responsable();
				System.out.println("Introduce DNI: ");
				String dni3 = input.nextLine();

				System.out.println("Introduce nombre: ");
				String nombre3 = input.nextLine();

				responsable3.setDni(dni3);
				responsable3.setNombre(nombre3);

				festival.altaClase1(responsable1.getDni(), responsable1.getNombre());
				festival.altaClase1(responsable2.getDni(), responsable2.getNombre());
				festival.altaClase1(responsable3.getDni(), responsable3.getNombre());

				System.out.println("Listamos responsables");
				festival.listarClase1();
				break;

			case 2:

				System.out.println("Buscamos un responsable");
				System.out.print("Introduce dni de responsable a buscar: ");
				String dniBuscar = input.nextLine();
				System.out.println("Buscamos responsable: " + dniBuscar);
				System.out.println(festival.buscarElemento(dniBuscar));

				break;
				
			case 3:

				System.out.println("Eliminamos un responsable");
				System.out.print("Introduce dni de responsable a eliminar: ");
				String dniEliminar = input.nextLine();
				System.out.println("Eliminar responsable" + dniEliminar);
				festival.eliminarClase1(dniEliminar);
				System.out.println("listamos responsables");
				festival.listarClase1();

				break;
			case 4:
				
				System.out.println("Listamos responsables");
				festival.listarClase1();
				
				break;
				
			case 5:

				System.out.println("Damos de alta a tres trabajos de clase2");

				System.out.println("Introduce primer trabajo");
				Trabajo trabajo1 = new Trabajo();
				System.out.println("Introduce nombre: ");
				String puesto = input.nextLine();

				System.out.println("Introduce presupuesto: ");
				Double presupuesto = input.nextDouble();

				trabajo1.setNombre(puesto);
				trabajo1.setPresupuesto(presupuesto);

				input.nextLine();
				System.out.println("Introduce segundo trabajo");
				Trabajo trabajo2 = new Trabajo();
				System.out.println("Introduce nombre: ");
				String puesto2 = input.nextLine();

				System.out.println("Introduce presupuesto: ");
				Double presupuesto2 = input.nextDouble();

				trabajo2.setNombre(puesto2);
				trabajo2.setPresupuesto(presupuesto2);

				input.nextLine();
				System.out.println("Introduce tercer trabajo");
				Trabajo trabajo3 = new Trabajo();
				System.out.println("Introduce nombre: ");
				String puesto3 = input.nextLine();

				System.out.println("Introduce presupuesto: ");
				Double presupuesto3 = input.nextDouble();

				input.nextLine();
				trabajo3.setNombre(puesto3);
				trabajo3.setPresupuesto(presupuesto3);

				System.out.println("Damos de alta a tres elementos de clase2");
				festival.altaClase2(trabajo1.getNombre(), trabajo1.getPresupuesto(), "2020-02-09");
				festival.altaClase2(trabajo2.getNombre(), trabajo2.getPresupuesto(), "2019-04-03");
				festival.altaClase2(trabajo3.getNombre(), trabajo3.getPresupuesto(), "2018-12-03");

				System.out.println("Listamos trabajos");
				festival.listarClase2();

				break;

			case 6:

				System.out.println("Buscamos un trabajo");
				System.out.print("Introduce trabajo a buscar: ");
				String trabajoBuscar = input.nextLine();
				System.out.println("Buscamos trabajo" + trabajoBuscar);
				System.out.println(festival.buscarElemento2(trabajoBuscar));

				break;

			case 7:

				System.out.println("Eliminamos un trabajo");
				System.out.print("Introduce trabajo a eliminar: ");
				String trabajoEliminar = input.nextLine();
				System.out.println("Eliminar trabajo" + trabajoEliminar);
				festival.eliminarClase2(trabajoEliminar);
				System.out.println("listamos trabajos");
				festival.listarClase2();

				break;

			case 8:

				System.out.println("Listamos trabajos a partir del nombre del trabajo");
				System.out.print("Introduce nombre del trabajo: ");
				String listarTrabajo = input.nextLine();
				festival.listarClase2(listarTrabajo);

				break;

			case 9:

				System.out.println("Asignamos un elemento que use Clase1 y Clase2");
				System.out.print("Introduce dni de Clase1: ");
				String asignarDni = input.nextLine();

				System.out.print("Introduce trabajo de Clase2: ");
				String asignarTrabajo = input.nextLine();
				festival.asignarResponsable(asignarDni, asignarTrabajo);

				break;

			case 10:

				System.out.println("Listamos elementos de Clase2 a partir de un elemento de Clase1");
				System.out.print("Introduce dni de algun Responsable:");
				String introducirDni = input.nextLine();
				festival.listarTrabajosDeResponsable(introducirDni);
			}

		}while(opcion>=1 || opcion<=10);
		
		
		input.close();
	}

}
