package clases;

import java.time.LocalDate;

public class Trabajo {

	//Atributos
	
	private String nombre;
	private double presupuesto;
	private LocalDate fechaConcesion;
	private Responsable responsableTrabajo;
	
	
	//Constructor
	
	public Trabajo() {
		
		nombre="";
		presupuesto=0;
		
	}
	public Trabajo(String nombre, double presupuesto) {
		this.nombre = nombre;
		this.presupuesto = presupuesto;
	}
	
	
	//Metodos get y set
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPresupuesto() {
		return presupuesto;
	}
	public void setPresupuesto(double presupuesto) {
		this.presupuesto = presupuesto;
	}
	public LocalDate getFechaConcesion() {
		return fechaConcesion;
	}
	public void setFechaConcesion(LocalDate fechaConcesion) {
		this.fechaConcesion = fechaConcesion;
	}
	
	
	public Responsable getResponsableTrabajo() {
		return responsableTrabajo;
	}

	public void setResponsableTrabajo(Responsable responsableTrabajo) {
		this.responsableTrabajo = responsableTrabajo;
	}


	//Metodo toString
	@Override
	public String toString() {
		return "Trabajo [nombre=" + nombre + ", presupuesto=" + presupuesto + ", fechaConcesion=" + fechaConcesion
				+ ", responsableTrabajo=" + responsableTrabajo + "]";
	}

	
	
	
	
	
	
}
