package clases;

import java.time.LocalDate;

import java.util.ArrayList;
import java.util.Iterator;

public class Festivales {

	// Atributos

	private ArrayList<Responsable> listaClase1;
	private ArrayList<Trabajo> listaClase2;

	// Constructor

	public Festivales() {
		listaClase1 = new ArrayList<Responsable>();
		listaClase2 = new ArrayList<Trabajo>();
	}

	// Metodo altaClase1

	public void altaClase1(String dni, String nombre) {
		if ((!existeResponsable(nombre))) {
			Responsable nuevoResponsable = new Responsable(dni, nombre);
			nuevoResponsable.setFechaContratacion(LocalDate.now());
			listaClase1.add(nuevoResponsable);
		} else {
			System.out.println("El responsable ya existe");
		}
	}

	public boolean existeResponsable(String nombre) {
		for (Responsable responsable : listaClase1) {
			if (responsable != null && responsable.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	// Metodo listarClase1

	public void listarClase1() {
		for (Responsable responsable : listaClase1) {
			if (responsable != null) {
				System.out.println(responsable);
			}
		}
	}

	// Buscar elemento1 clase1
	public Responsable buscarElemento(String dni) {
		for (Responsable responsable : listaClase1) {
			if (responsable != null && responsable.getDni().equals(dni)) {
				return responsable;
			}
		}
		return null;
	}

	// Eliminar elemento clase1

	public void eliminarClase1(String dni) {

		Iterator<Responsable> iteradorClase1 = listaClase1.iterator();

		while (iteradorClase1.hasNext()) {
			Responsable responsable = iteradorClase1.next();
			if (responsable.getDni().equals(dni)) {
				iteradorClase1.remove();
			}
		}
	}

	// Metodo altaClase2

	public void altaClase2(String nombre, double presupuesto, String fechaConcesion) {
		if ((!existeTrabajo(nombre))) {
			Trabajo nuevoTrabajo = new Trabajo(nombre, presupuesto);
			nuevoTrabajo.setFechaConcesion(LocalDate.parse(fechaConcesion));
			listaClase2.add(nuevoTrabajo);
		} else {
			System.out.println("El responsable ya existe");
		}
	}

	public boolean existeTrabajo(String nombre) {
		for (Trabajo trabajo : listaClase2) {
			if (trabajo != null && trabajo.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	// Eliminar elemento clase2

	public void eliminarClase2(String nombre) {

		Iterator<Trabajo> iteradorClase2 = listaClase2.iterator();

		while (iteradorClase2.hasNext()) {
			Trabajo trabajo = iteradorClase2.next();
			if (trabajo.getNombre().equals(nombre)) {
				iteradorClase2.remove();
			}
		}
	}

	// Buscar elemento1 clase2
	public Trabajo buscarElemento2(String nombre) {
		for (Trabajo trabajo : listaClase2) {
			if (trabajo != null && trabajo.getNombre().equals(nombre)) {
				return trabajo;
			}
		}
		return null;
	}

	
	// Metodo listarClase1

		public void listarClase2() {
			for (Trabajo trabajo : listaClase2) {
				if (trabajo != null) {
					System.out.println(trabajo);
				}
			}
		}

	// Listar clase2 por algun atributo
	public void listarClase2(String nombre) {
		for (Trabajo trabajo : listaClase2) {
			if (trabajo.getNombre().equals(nombre)) {
				System.out.println(trabajo);
			}
		}
	}

	// Listar clase2 por algun atributo con un elmentno de clase 1
	
	public void listarTrabajosDeResponsable(String dni) {
		for (Trabajo trabajo : listaClase2) {
			if (trabajo.getResponsableTrabajo() != null && trabajo.getResponsableTrabajo().getDni().equals(dni)) {
				System.out.println(trabajo);
			}
		}
	}

	// Asignar elemento de clase 1 a clase 2

	public void asignarResponsable(String dni, String nombreTrabajo) {
		if (buscarElemento(dni) != null && buscarElemento2(nombreTrabajo) != null) {
			Responsable jefe = buscarElemento(dni);
			Trabajo trabajo = buscarElemento2(nombreTrabajo);
			trabajo.setResponsableTrabajo(jefe);
		}
	}


}
